from django.shortcuts import render, get_object_or_404, redirect
from todo.models import TodoList, TodoItem
from todo.forms import TodoListForm, TodoItemForm
# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list_list": todos, }
    return render(request, "todo/list.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": list}
    return render(request, "todo/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {"form": form, }
    return render(request, "todo/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "todo_detail": list,
        "form": form,
    }
    return render(request, "todo/modify.html", context)


def todo_list_delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todo/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail", id=todos.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form, }
    return render(request, "todo/createitem.html", context)